#!/bin/env bash
if command -v apt-get >/dev/null; then
  read -p "Install deps (root required) [y/n]" -n 1 -r
  echo
  if [[ $REPLY =~ ^[Yy]$ ]]
  then
    apt-get install -y wget git ansible
    wget https://releases.hashicorp.com/vagrant/2.2.9/vagrant_2.2.9_x86_64.deb -O /tmp/vagrant.deb
    apt install /tmp/vagrant.deb
  fi

  ansible_vars="wg_type=client"

  PRIVKEY=$(wg genkey)
  PUBKEY=$(echo "$PRIVKEY" | wg pubkey)
  echo "Client Private Key: $PRIVKEY"
  echo "Cleint Public Key: $PUBKEY"

  ansible_vars="${ansible_vars} wg_client_priv=${PRIVKEY}"

  read -p "Enter wireguard client ip: "
  ansible_vars="${ansible_vars} wg_client_addr=${REPLY} wg_subnet_prefix=$(echo $REPLY | cut -d '.' -f'1 2 3')"
  wg_client_ip=$REPLY
  read -p "Enter Server Endpoint: "
  ansible_vars="${ansible_vars} wg_endpoint=${REPLY}"

  read -p "Enter Server Public Key: "
  ansible_vars="${ansible_vars} wg_server_pub=${REPLY}"

  read -p "Enter Interface name with out .conf: "
  ansible_vars="${ansible_vars} wg_interface=${REPLY}"
  echo "Copy this to your wireguard server"
  echo "+++++++++++++++++++++++"
  echo "[Peer]"
  echo "AllowedIPs = $wg_client_ip/32"
  echo "PublicKey = $PUBKEY"
  echo "+++++++++++++++++++++++"


  read -p "Start Setup? [y/n]" -n 1 -r
  echo
  if [[ $REPLY =~ ^[Yy]$ ]]
  then
    git clone https://gitlab.com/linalinn/ansible-mk8s-quick.git /tmp/ansible_vagrant_vbox_k8s
    cd /tmp/ansible_vagrant_vbox_k8s
    echo "-e \"$ansible_vars\"" > ANSIBLE_CONFIG
    ANSIBLE_CONFIG=$ansible_vars vagrant up
  fi
else
  echo "I have no Idea what im doing here no apt-get found"
fi