# Ansible Mk8s Quick with wireguard
This repo contains scripts to setup multi micro k8s clusters connected via wireguard.

# Microk8s on Blech (wireguard server/client)
this script musst be run as root
```bash
curl https://gitlab.com/linalinn/ansible-mk8s-quick/-/raw/main/setup.sh > /tmp/setup.sh
sudo bash /tmp/setup.sh
```

# Microk8s in VirtualBox via vagrant (wireguard client only)
this script musst not run as root if you already have wget, git, ansible and vagrant installed
```bash
source <(curl https://gitlab.com/linalinn/ansible-mk8s-quick/-/raw/main/vagrant-vbox-setup.sh)
```
