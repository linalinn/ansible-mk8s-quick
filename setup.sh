#!/bin/env bash
if command -v apt-get >/dev/null; then
  apt-get install -y wireguard ansible git snapd

  ansible_vars=""
  read -p "Is this your wireguard server? [y/n]: " -n 1 -r
  echo
  if [[ $REPLY =~ ^[Yy]$ ]]
  then
    ansible_vars="wg_type=server "

    PRIVKEY=$(wg genkey)
    PUBKEY=$(echo "$PRIVKEY" | wg pubkey)
    echo "Server Private Key: $PRIVKEY"
    echo "Server Public Key: $PUBKEY"

    ansible_vars="${ansible_vars} wg_server_priv=${PRIVKEY} "

    read -p "Enter wireguard subnet prefix like 10.1.0: "
    echo
    ansible_vars="${ansible_vars} wg_subnet_prefix=${REPLY} "

    read -p "Enter wireguard port: "
    ansible_vars="${ansible_vars} wg_port=${REPLY}"

    read -p "Interface name with out .conf: "
    ansible_vars="${ansible_vars} wg_interface=${REPLY}"
  else
    ansible_vars="wg_type=client"

    PRIVKEY=$(wg genkey)
    PUBKEY=$(echo "$PRIVKEY" | wg pubkey)
    echo "Client Private Key: $PRIVKEY"
    echo "Cleint Public Key: $PUBKEY"

    ansible_vars="${ansible_vars} wg_client_priv=${PRIVKEY}"

    read -p "Enter wireguard client ip: "
    echo
    ansible_vars="${ansible_vars} wg_client_addr=${REPLY} wg_subnet_prefix=$(echo $REPLY | cut -d '.' -f'1 2 3')"
    wg_client_ip=$REPLY

    read -p "Enter Server Endpoint: "
    ansible_vars="${ansible_vars} wg_endpoint=${REPLY}"

    read -p "Enter Server Public Key: "
    ansible_vars="${ansible_vars} wg_server_pub=${REPLY}"

    read -p "Enter Interface name with out .conf: "
    ansible_vars="${ansible_vars} wg_interface=${REPLY}"
    echo "Copy this to your wireguard server"
    echo "+++++++++++++++++++++++"
    echo "[Peer]"
    echo "AllowedIPs = $wg_client_ip/32"
    echo "PublicKey = $PUBKEY"
    echo "+++++++++++++++++++++++"

  fi


  read -p "Start Setup? [y/n]" -n 1 -r
  echo
  if [[ $REPLY =~ ^[Yy]$ ]]
  then
    ansible-pull $1 -U https://gitlab.com/linalinn/ansible-mk8s-quick.git -c local -e "$ansible_vars" playbook.yml
  fi
else
  echo "I have no Idea what im doing here no apt-get found"
fi